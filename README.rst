Install::

    pip install envzigzag

Usage::

    # Snapshot one state
    envzig

    # Change things, or switch to a different terminal, then compare:
    envzag

When you run the second command, it shows what has changed. It might look
something like this::

    Removed [in zig, not zag]:
      CONDA_DEFAULT_ENV=envzig-test

    Added   [in zag, not zig]:
      OLDPWD=/home/me/Code

    Changed:
      PWD=
      ├zig: /home/takluyver/Code/flit
      └zag: /home/takluyver/Code/envzigzag
